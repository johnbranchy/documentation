---
sidebar_position: 12
title: Dashboard
id: dashboard
---
import Alert from "../src/components/Alert";

Use the Radar [dashboard](https://radar.com/dashboard) to manage, explore, and analyze your Radar data, such as [geofences](/geofences), [users](https://radar.com/dashboard/users), [events](https://radar.com/dashboard/events), and [trips](/trip-tracking).

The [dashboard](https://radar.com/dashboard) can also be used to configure [integrations](https://radar.com/dashboard/integrations), [project settings](https://radar.com/dashboard/settings) such as enabled features (e.g., [places](/places)) and event types (e.g., `user.exited_place`), and [custom data retention windows](https://radar.com/dashboard/privacy).

## Simulator

The [simulator](https://radar.com/dashboard/simulate) is the easiest way to generate location updates to test and visualize entry and exit events at will. Gone are the days of needing to drive around or download spoofing apps to try out location-enabled features. The simulator acts as a playground to experiment with our API without any code!

### How it works

1. **Click around the map to generate locations and events.** The activity feed is populated in real-time to mirror how our APIs would surface locations and events in your app. The data generated flows through to your enabled [integrations](https://radar.com/dashboard/integrations) in the corresponding environment. The locations and events generated abide by your project's [settings](https://radar.com/dashboard/settings) and will persist across the dashboard. To avoid any production side effects, use the test environment with the default user provided (`simulated-user-id`).

2. **Simulate a target user.** To simulate a given [user](/api#users), click into that specific user on the [users](https://radar.com/dashboard/users) page and hit the "Simulate" button. You can also set custom user metadata, app-specific information that downstream systems can act upon, such as whether a user has been activated or membership in a target cohort.

3. **Simulate a trip.** There are 3 fields necessary to simulate a [trip](/trip-tracking): `externalId`, `destinationGeofenceTag`, and `destinationGeofenceExternalId`. You can enter them manually or select a geofence on the map and click "Set as trip destination" to autofill these values. Then, click "Start Trip" to officially kick off and activate the trip! As you click around, you'll notice events such as `user.approaching_trip_destination` and `user.arrived_at_trip_destination`.

![Simulator.gif](/gif/mock_tracking.gif)

## User Activity

The User Activity page is the most straightforward way to debug a user's recent locations and events. Navigate to the [Users page](https://radar.com/dashboard/users) and select *View* within the desired user row. Then, click *View Activity*. Filter the user's recent activity down to the minute to investigate why events did or did not fire, and understand [confidence levels](/geofences#confidence-and-accuracy) for events in real-world scenarios.

### How it works

1. **See a unified view of locations, events, and geofences.** The User Activity map displays a user's recent locations and events alongside geofences to pin down exactly when entries and exits occurred.

2. **Focus on an individual location or event.** Hover over a row in the Activity Feed to center the map view around those coordinates and to display the accuracy bubble for that location or event. An Event's [confidence level](/geofences#confidence-and-accuracy) is a function of the accuracy of the location and the geometry of the geofence. The smaller the geofence and the less accurate the location, the lower the confidence.

3. **Filter to a specific time period.** Move the bounds of the slider to isolate a range of time for investigation. The slider supports granularity down to the minute.

![UserActivity.gif](/gif/user_activity.gif)

### Location sources
- **`FOREGROUND_LOCATION`**: location update triggered by `Radar.trackOnce`
- **`BACKGROUND_LOCATION`**: location updated triggered by `Radar.startTracking`
- **`GEOFENCE_ENTER`**: location update triggered by a client-side geofence entry, either nearby geofences synced from the server or a "bubble" geofence around the device's current location
- **`GEOFENCE_EXIT`**: location updated triggered by client-side geofence exit, either nearby geofences synced from the server or a "bubble" geofence around the device's current location
- **`BEACON_ENTER`**: location update triggered by a client-side beacon entry
- **`BEACON_EXIT`**: location update triggered by a client-side beacon exit
- **`VISIT_ARRIVAL`** (iOS only): location update triggered by an arrival from the [iOS visit monitoring service](https://developer.apple.com/documentation/corelocation/getting_the_user_s_location/using_the_visits_location_service)
- **`VISIT_DEPARTURE`** (iOS only): location update triggered by a departure [iOS visit monitoring service](https://developer.apple.com/documentation/corelocation/getting_the_user_s_location/using_the_visits_location_service)
- **`MANUAL_LOCATION`**: location update triggered by `Radar.trackOnce` passing in a location not collected by the Radar SDK
- **`MOCK_LOCATION`**: location update triggered by `Radar.mockTracking`

## Place matching

<Alert alertType="info">
  Place matching is available on the {` `}
  <a href="https://radar.com/pricing" target="_blank">Enterprise plan</a>
  .
</Alert>

Place matching is the simplest way to make sure your geofence coordinates and geometries are accurate and stay up-to-date over time. Cleaning your geofence datasets via Radar's import process will reduce false positives. Entry and exit events will be more reliable, trip approaching and arrived signals will be more accurate, and messaging for nearby locations will be more relevant.

Navigate to the [geofences](https://radar.com/dashboard/geofences) page and select *Import*. If [places](/places) are enabled for your project, opt-in to this feature by toggling on *Enable place matching*. Select [chains](/places/chains) to match your geofences against. The import process will attempt to match each geofence against Radar's places for those chains.

### How it works

1. **Import your geofences against Radar's places data.** Upon importing your geofences into Radar, select [chains](/places/chains) to match your geofences against. By default, Radar will search for [places](/places) for those chains within 10 kilometers (~6 miles) of each geofence and match to the nearest location. The search radius can be configured as an advanced option at the time of import.

2. **View the results of the place matching import.** After the import has completed, examine the results on the [import history](https://radar.com/dashboard/geofences/imports) page. Click *View import* to see a summary as well as logs that indicate the outcome for each row. If the place match was successful, the import log will contain details such as a link to the Radar place and the distance that the geofence center was corrected. Select *Expand all* to display the input data and a map UI representing the transformation.

3. **Filter and export the results for further analysis.** To perform more granular investigations, apply *Filters* to isolate rows that were not place matched, geofences that have large center correction values, or errors. Click *Export rows* to download a CSV of the filtered results to perform additional analysis in other tools.

![PlaceMatching.gif](/gif/place_matching.gif)

### Geofence types

#### Circle
By default, when a **circle** geofence is matched, the geofence will take on the geometry of the Radar place. As an advanced option, a geofence can adopt the center of the Radar place only, while retaining its original geometry. This behavior can be toggled by editing the geofence after the import.

![Place matching circle](/img/place-matching/place_matching_circle.png)

#### Isochrone
When an **isochrone** geofence is matched, the geofence will only adopt the center of the Radar place, regenerating the [isochrone](https://en.wikipedia.org/wiki/Isochrone_map) geometry from the new center.

![Place matching isochrone](/img/place-matching/place_matching_isochrone.png)

#### Polygon
When a **polygon** geofence is matched, the geofence will be linked to the Radar place, but not adopt the place center or geometry. This behavior supports geofences that represent areas such as parking lots and drive-thru lanes.

![Place matching polygon](/img/place-matching/place_matching_polygon.png)

### Advanced options

- **Search radius.** By default, place matching will look for the nearest location within 10 kilometers, the maximum radius provided by the [search places](/api#search-places) endpoint. This value can be reduced to enforce a tighter search distance.
- **Geofence tag(s).** Filter the rows for which place matching is attempted. For example, only perform place matching on rows where the _tag_ is `store` but not `parking lot`.
- **Use Radar's place geometry.** By default, if a place match is found, [circle geofences](/dashboard#circle) will adopt both the center and geometry of the Radar place. This behavior can be changed so that circle geofences only take on the center but not the geometry.
